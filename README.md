## In.IoT Moquette

In.IoT uses a modified version of Moquette that works with other In.IoT modules.

With this modified version of Moquette, publishing and subscribing to topics follows certain rules. These rules were implemented a security feature to avoid devices from other applications from "eavesdropping" unauthorized communications.

[You can find the user guide here](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/Usage%20Guide)


[You can find installation instructions in our website](https://inatel.br/in-iot/)



Or in our Wiki:

* [Ubuntu](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/installation%20(Ubuntu))
* Windows (Comming soon)
* MAC (Comming soon)
* CentOS (Comming soon)

[To secure your deployment and scale the solution with Microservices access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/scaling%20and%20securing%20the%20solution)
[To configure your MQTT cluster access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/Configuring%20Multiple%20MQTT%20Brokers)

If you are a developer, you can find instructions [here](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/Developer%20Guide%20(Eclipse))

## Ports used by other In.IoT services 

| Application   				| Port 			|
| ------------- 				| ------------- |
| Device API  					| 8070  		|
| Admin GUI						| 8100  		|
| Admin REST API  				| 8190  		|
| MQTT Broker  					| 1883  		|
| Eureka Service discovery  	| 8761  		|
| Zuul Gateway  				| 8090  		|
| MQTT Proxy  					| 5000  		|
| CoAP Server  					| 5683  		|
| CoAP Proxy  					| 5001  		|



## What is Moquette?

[![Build Status](https://api.travis-ci.org/andsel/moquette.svg?branch=master)](https://travis-ci.org/andsel/moquette)

* [Documentation reference guide](http://andsel.github.io/moquette/) Guide on how to use and configure Moquette
* [Google Group](https://groups.google.com/forum/#!forum/moquette-mqtt) Google Group to participate in development discussions.

Moquette aims to be a MQTT compliant broker. The broker supports QoS 0, QoS 1 and QoS 2.

Its designed to be evented, uses Netty for the protocol encoding and decoding part.