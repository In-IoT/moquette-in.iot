package io.moquette.databaseAccessANDHTTPMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseUtils {

	private static final String DB_DRIVER = "org.h2.Driver";
	private static final String DB_CONNECTION = "jdbc:h2:mem:devices;DB_CLOSE_DELAY=-1";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";

	public DatabaseUtils() {
		// TODO Auto-generated constructor stub
	}

	public Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	public void createTableDevices(Connection connection) {

		PreparedStatement createPreparedStatement = null;

		String CreateQuery = "CREATE TABLE IF NOT EXISTS DEVICE( clientid varchar(40) primary key ,username varchar(40), application varchar(40), password varchar(40), token varchar(230), noBroadcast int )";

//		String CreateQuery = "CREATE TABLE IF NOT EXISTS DEVICE( username varchar(40) primary key, application varchar(40), password varchar(40))";

		try {
			connection.setAutoCommit(false);

			createPreparedStatement = connection.prepareStatement(CreateQuery);
			createPreparedStatement.executeUpdate();
			createPreparedStatement.close();

			connection.commit();
		} catch (SQLException e) {
			System.out.println("Exception Message " + e.getLocalizedMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

//	public void insertDataDevices(String username, String password, String application, Connection connection) {
	public void insertDataDevices(String clientId, String username, String password, String application, String token, Connection connection, int noBroadcast) {
		PreparedStatement insertPreparedStatement = null;
		String InsertQuery = "REPLACE INTO DEVICE" + "(clientid, username , application, password, token, noBroadcast) values" + "(?,?,?,?,?,?)";

		try {

			insertPreparedStatement = connection.prepareStatement(InsertQuery);
			insertPreparedStatement.setString(1, clientId);
			insertPreparedStatement.setString(2, username);
			insertPreparedStatement.setString(3, application);
			insertPreparedStatement.setString(4, password);
			insertPreparedStatement.setString(5, token);
			insertPreparedStatement.setInt(6, noBroadcast);
			insertPreparedStatement.executeUpdate();
			insertPreparedStatement.close();
			connection.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public void deleteClient(String clientId, Connection connection) {
		PreparedStatement deletePreparedStatement = null;
		String deleteQuery = "DELETE FROM DEVICE WHERE clientid=?";

		try {

			deletePreparedStatement = connection.prepareStatement(deleteQuery);
			deletePreparedStatement.setString(1, clientId);
			deletePreparedStatement.executeUpdate();
			deletePreparedStatement.close();
			connection.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public User selectApplicationAndUsernameAndToken(String clientId, Connection connection) {

		User user = null;
		try {
			user = new User();

			PreparedStatement statement = connection
					.prepareStatement("SELECT application, token, username, noBroadcast FROM DEVICE WHERE clientid=?");
			statement.setString(1, clientId);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				user.setApplication(rs.getString("application"));
				user.setToken(rs.getString("token"));
				user.setUsername(rs.getString("username"));
				user.setNoBroadcast(rs.getInt("noBroadcast"));
			}
			statement.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;

	}

	public String selectApplication(String clientid, Connection connection) {

		String application = "";
		PreparedStatement statement;
		try {
			statement = connection
					.prepareStatement("SELECT application FROM DEVICE WHERE clientid=?");
			statement.setString(1, clientid);

			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				application = rs.getString("application");
			}
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return application;

	}
	
	public List<User> selectClientsByApplication(String application, Connection connection) {

		//String application = "";
		List<User> users = new ArrayList<>();
		PreparedStatement statement;
		User user = null;
		try {
			//user = new User();
			statement = connection
					//.prepareStatement("SELECT username FROM DEVICE WHERE application=?");
					.prepareStatement("SELECT COUNT(DISTINCT clientid) AS count_user, username FROM DEVICE WHERE application=? GROUP BY username");
			statement.setString(1, application);
//			SELECT COUNT(DISTINCT program_name) AS Count,
//			  program_type AS [Type] 
//					  FROM cm_production 
//					  WHERE push_number=@push_number 
//					  GROUP BY program_type

			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				user = new User();
				//user.setApplication(rs.getString("application"));
				//user.setPassword(rs.getString("password"));
				user.setUsername(rs.getString("username"));
				user.setnClients(rs.getInt("count_user"));
				//user.setClientID(rs.getString("clientid"));
				//System.out.println(rs.getString("count_user"));
				users.add(user);
				
			}
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return users;

	}
	
	public List<User> selectClientsByApplicationAndUsername(String application, String username, Connection connection) {

		//String application = "";
		List<User> users = new ArrayList<>();
		PreparedStatement statement;
		User user = null;
		try {
			//user = new User();
			statement = connection
					.prepareStatement("SELECT username, clientid FROM DEVICE WHERE application=? AND username=?");
			statement.setString(1, application);
			statement.setString(2, username);

			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				user = new User();
				//user.setApplication(rs.getString("application"));
				//user.setPassword(rs.getString("password"));
				user.setUsername(rs.getString("username"));
				user.setClientID(rs.getString("clientid"));
				
				users.add(user);
				
			}
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return users;

	}
	
	
	public User selectClientsByApplicationAndClientId(String application, String clientId, Connection connection) {

		//String application = "";
		//List<User> users = new ArrayList<>();
		PreparedStatement statement;
		User user = null;
		try {
			//user = new User();
			statement = connection
					.prepareStatement("SELECT username, clientid FROM DEVICE WHERE application=? AND clientId=?");
			statement.setString(1, application);
			statement.setString(2, clientId);

			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				user = new User();
				//user.setApplication(rs.getString("application"));
				//user.setPassword(rs.getString("password"));
				user.setUsername(rs.getString("username"));
				user.setClientID(rs.getString("clientid"));
				
				//users.add(user);
				
			}
			statement.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return user;

	}
	

}
