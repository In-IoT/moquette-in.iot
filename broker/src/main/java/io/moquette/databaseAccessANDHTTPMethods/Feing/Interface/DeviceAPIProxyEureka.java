package io.moquette.databaseAccessANDHTTPMethods.Feing.Interface;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.moquette.databaseAccessANDHTTPMethods.Credentials;
import io.moquette.databaseAccessANDHTTPMethods.User;
import io.moquette.model.DeviceList;

//@Repository("adminAPIProxyNoEureka")
@FeignClient(name="device-api")
public interface DeviceAPIProxyEureka {
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/auth/device/signin", consumes = "application/json")
    ResponseEntity<String> login(Credentials credentials);
	
	@RequestMapping(method = RequestMethod.GET, value = "/api/v1/auth/device/me", consumes = "application/json")
    User verifyJWT(@RequestHeader("Authorization") String token);
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/message", consumes = "application/json")
    void sendMessage(@RequestHeader("Authorization") String token, String message);
	
	@RequestMapping(method = RequestMethod.GET,value = "/api/v1/devices/findAbility"+"/{deviceAbility}", consumes = "application/json")
	DeviceList findAbility(@RequestHeader("Authorization") String token, @PathVariable(name = "deviceAbility", required = true) String deviceAbility);
	
	@RequestMapping(method = RequestMethod.GET,value = "/api/v1/devices/findAbility"+"/{deviceAbility}/{order}", consumes = "application/json")
	String findAbilityAsString(@RequestHeader("Authorization") String token, @PathVariable(name = "deviceAbility", required = true) String deviceAbility,
			@PathVariable(name = "order", required = true) String order);
	
	

}
