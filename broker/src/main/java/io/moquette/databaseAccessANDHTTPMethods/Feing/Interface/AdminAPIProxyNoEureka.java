package io.moquette.databaseAccessANDHTTPMethods.Feing.Interface;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.moquette.databaseAccessANDHTTPMethods.Credentials;
import io.moquette.databaseAccessANDHTTPMethods.User;

//@Repository("adminAPIProxyNoEureka")
@FeignClient(name="admin-api",url="localhost:8190")
public interface AdminAPIProxyNoEureka {
	
	@RequestMapping(method = RequestMethod.POST, value = "/login", consumes = "application/json")
    ResponseEntity<String> login(Credentials credentials);
	
	@RequestMapping(method = RequestMethod.GET, value = "/isJWTValid", consumes = "application/json")
    User verifyJWT(@RequestHeader("Authorization") String token);
//	
//	@RequestMapping(method = RequestMethod.POST, value = "/api/v1/message", consumes = "application/json")
//    void sendMessage(@RequestHeader("Authorization") String token, String message);
//	
//	@RequestMapping(method = RequestMethod.GET,value = "/api/v1/devices/findAbility"+"/{deviceAbility}", consumes = "application/json")
//	DeviceList findAbility(@RequestHeader("Authorization") String token, @PathVariable(name = "deviceAbility", required = true) String deviceAbility);
//	
	@RequestMapping(method = RequestMethod.GET,value = "/findDeviceAbility"+"/{deviceAbility}/{order}", consumes = "application/json")
	String findAbility(@RequestHeader("Authorization") String token, @PathVariable(name = "deviceAbility", required = true) String deviceAbility,
			@PathVariable(name = "order", required = true) String order);
	

}
