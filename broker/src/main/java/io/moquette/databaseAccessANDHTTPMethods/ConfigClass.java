package io.moquette.databaseAccessANDHTTPMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.cloud.client.discovery.DiscoveryClient;

import io.moquette.databaseAccessANDHTTPMethods.Feing.Interface.AdminAPIProxyEureka;
import io.moquette.databaseAccessANDHTTPMethods.Feing.Interface.AdminAPIProxyNoEureka;
import io.moquette.databaseAccessANDHTTPMethods.Feing.Interface.DeviceAPIProxyEureka;
import io.moquette.databaseAccessANDHTTPMethods.Feing.Interface.DeviceAPIProxyNoEureka;

@Component("ConfigClass")
public class ConfigClass {
	
	@Autowired
	public static AdminAPIProxyNoEureka adminAPIProxyNoEureka;
	
	@Autowired
	public static AdminAPIProxyEureka adminAPIProxyEureka;
	
	@Autowired
	public static DeviceAPIProxyNoEureka deviceAPIProxyNoEureka;
	
	@Autowired
	public static DeviceAPIProxyEureka deviceAPIProxyEureka;
	
	@Autowired
    public static DiscoveryClient discoveryClient;
	
	public static String forwarPort;
	public static int runningPort;
	public static boolean isMultipleMQTTInstances;
	public static ArrayList<ForwardAddressAndPort> forwardAddressAndPort = new ArrayList<>();
	
	public static String dashboardREST;
	public static String DeviceREST;
	
	public static String BrokerUsername;
	public static String BrokerPassword;
	
	public static String restSignInURL;
	public static String dashboardSignInURL;
	public static String restPublishURL;
	
	public static String ipAddress;
	
	public static String PREFIX;
	
	@Autowired
    public ConfigClass(AdminAPIProxyNoEureka adminAPIProxyNoEureka, 
    				   AdminAPIProxyEureka adminAPIProxyEureka,
			    	   DeviceAPIProxyNoEureka deviceAPIProxyNoEureka,
			    	   DeviceAPIProxyEureka deviceAPIProxyEureka,
			    	   DiscoveryClient discoveryClient){
		ConfigClass.adminAPIProxyNoEureka = adminAPIProxyNoEureka;
		ConfigClass.deviceAPIProxyNoEureka = deviceAPIProxyNoEureka;
		ConfigClass.adminAPIProxyEureka = adminAPIProxyEureka;
		ConfigClass.deviceAPIProxyEureka = deviceAPIProxyEureka;
		ConfigClass.discoveryClient = discoveryClient;
		
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "application.properties";
		
		try {
			inputStream = new FileInputStream (System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
    	String prefixAux = prop.getProperty("eureka.client.register-with-eureka", "true");
    	if(prefixAux.equals("true")) {
    		PREFIX = "/admin-gui";
    	} else {
    		PREFIX = "";
    	}
    }
	
	
	

}
