package io.moquette.databaseAccessANDHTTPMethods;

public class CustomResponse {
	
	private String application;
	private int responseCode;
	
	CustomResponse(){
		
	}
	
	public CustomResponse(String application, int responseCode) {
		super();
		this.application = application;
		this.responseCode = responseCode;
	}



	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	
}
