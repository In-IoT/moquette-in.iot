package io.moquette.databaseAccessANDHTTPMethods;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class EurekaMethods {

	public boolean registerMQTTInstance(String ip, String port) {

		StringEntity loginString = null;

		try {
			loginString = new StringEntity("{" + "\"instance\": {\n" + "\"hostName\": \"" + ip + ":" + port + "\","
					+ "\"app\": \"mqtt\"," + "\"ipAddr\": \"" + ip + "\"," + "\"status\": \"UP\","
					+ "\"port\": {\"$\": \"" + port + "\", \"@enabled\": \"true\"}," + "\"statusPageUrl\": \"http://"
					+ ip + ":" + port + "/status\"," + "\"homePageUrl\": \"http://" + ip + ":" + port + "\","
					+ "\"dataCenterInfo\": {\n"
					+ "\"@class\": \"com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo\", \n"
					+ "\"name\": \"MyOwn\"\n" + "}\n" + "}" + "}");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		CloseableHttpClient client = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost("http://localhost:8761/eureka/apps/mqtt");
		httpPost.setHeader("Content-Type", "application/json");
		
		String auth = "eureka:password";
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		httpPost.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		 
		//HttpClient client = HttpClientBuilder.create().build();
		//HttpResponse response = client.execute(request);
		
		httpPost.setEntity(loginString);

		HttpResponse httpResponse = null;

		try {
			httpResponse = client.execute(httpPost);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if (httpResponse.getStatusLine().getStatusCode() != 204) {
			try {
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		try {
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	public void getMQTTInstances() {

		CloseableHttpClient client;
		ArrayList<ForwardAddressAndPort> forwardAddressAndPorts;
		forwardAddressAndPorts = new ArrayList<>();

		client = HttpClients.createDefault();

		HttpGet httpGet = new HttpGet("http://localhost:8761/eureka/apps/mqtt");
		httpGet.setHeader("Accept", "application/json");

		
		String auth = "eureka:password";
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		httpGet.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		
		HttpResponse httpResponse = null;

		try {
			httpResponse = client.execute(httpGet);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(httpResponse.getStatusLine().getStatusCode() == 200) {

		HttpEntity entity = httpResponse.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
		} catch (ParseException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// System.out.println(responseString);

		String str = "{ \"instance\":"
				+ responseString.substring(responseString.indexOf("["), responseString.lastIndexOf("]"))
				+ "]}";
		JSONObject jsonObject = new JSONObject(str);

		JSONArray lineItems = jsonObject.getJSONArray("instance");
		for (Object o : lineItems) {
			JSONObject jsonLineItem = (JSONObject) o;
			String address = jsonLineItem.getString("ipAddr");
			JSONObject jsonObjectPort = jsonLineItem.getJSONObject("port");

			int port = jsonObjectPort.getInt("$");

			if(port != ConfigClass.runningPort) {
				ForwardAddressAndPort forwardAddressAndPort = new ForwardAddressAndPort(address, port);
				forwardAddressAndPorts.add(forwardAddressAndPort);
			}
		}

		try {
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConfigClass.forwardAddressAndPort = new ArrayList<>(forwardAddressAndPorts);
		
		}

	}

}
