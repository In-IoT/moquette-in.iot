package io.moquette.model;

import java.util.List;

import io.moquette.databaseAccessANDHTTPMethods.User;

public class UserList {

	private List<User> users;
	
	public UserList(List<User> users) {
		this.users = users;
	}
	

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	
	
	
}
