package io.moquette.model;

import java.util.Date;
import java.util.List;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class Device {
    private String id;
    private String username;
    private String name;
    private String password;
    private String pictureUrl;
    private String type;
    private String createBy;
    private String mac;
    private String ip;
    private String description;
//    private List<Message> messages;
    private List<String> groups;
    private boolean publicDevice;
    private boolean deleted;
    private Date createDate;
    private String application;
    private List<String> abilities;
    private List<String> keywords;
    private String model;
    private Object abilityDetails;
    private List<String> variables;

    public Device(
            @JsonProperty("id") String id,
            @JsonProperty("username") String username,
            @JsonProperty("name") String name,
            @JsonProperty("password") String password,
            @JsonProperty("pictureUrl") String pictureUrl,
            @JsonProperty("type") String type,
            @JsonProperty("createBy") String createBy,
            @JsonProperty("mac") String mac,
            @JsonProperty("ip") String ip,
            @JsonProperty("description") String description,
//            @JsonProperty("messages") List<Message> messages,
            @JsonProperty("groups") List<String> groups,
            @JsonProperty("publicDevice") boolean publicDevice,
            @JsonProperty("deleted") boolean deleted,
            @JsonProperty("createDate") Date createDate,
            @JsonProperty("application") String application,
            @JsonProperty("abilities") List<String> abilities,
            @JsonProperty("keywords") List<String> keywords,
            @JsonProperty("model") String model,
            @JsonProperty("abilityDetails") Object abilityDetails
    ) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.pictureUrl = pictureUrl;
        this.type = type;
        this.createBy = createBy;
        this.mac = mac;
        this.ip = ip;
        this.description = description;
//        this.messages = messages;
        this.groups = groups;
        this.publicDevice = publicDevice;
        this.deleted = deleted;
        this.createDate = createDate;
        this.application = application;
        this.abilities = abilities;
        this.keywords = keywords;
        this.model = model;
        this.abilityDetails = abilityDetails;
    }
    
    public Device() {
    	
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }
    
//    public List<Message> getMessages() {
//		return messages;
//	}
//
//	public void setMessages(List<Message> messages) {
//		this.messages = messages;
//	}

    public boolean isPublicDevice() {
        return publicDevice;
    }

    public void setPublicDevice(boolean publicDevice) {
        this.publicDevice = publicDevice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public List<String> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<String> abilities) {
		this.abilities = abilities;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Object getAbilityDetails() {
		return abilityDetails;
	}

	public void setAbilityDetails(Object abilityDetails) {
		this.abilityDetails = abilityDetails;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}

}
