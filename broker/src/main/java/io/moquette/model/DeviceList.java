package io.moquette.model;

import java.util.List;


public class DeviceList {

	private List<Device> devices;
	
	public DeviceList() {
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	
}