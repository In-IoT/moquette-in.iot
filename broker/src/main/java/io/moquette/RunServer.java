package io.moquette;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.moquette.server.Server;


//@Configuration
@SpringBootApplication
@EnableFeignClients
//@EnableDiscoveryClient
public class RunServer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunServer.class);
	}

	
	public static void main(String[] args) throws IOException {
		checkIfConfigFileExists();
    	SpringApplication.run(RunServer.class, args);
        final Server server = new Server();
        server.startServer();
        System.out.println("Server started, version 0.12-SNAPSHOT");
        //Bind  a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(server::stopServer));
    }
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("server.port = 1883\n" + 
						"spring.main.web-application-type=NONE \n" + 
						"spring.application.name= mqtt-instances\n" + 
						"\n" + 
						"\n" + 
						"logging.level.root=ERROR\n" + 
						"logging.level.org.springframework.web=ERROR\n" + 
						"logging.level.org.hibernate=ERROR\n" + 
						"\n" + 
						"eureka.instance.preferIpAddress=true\n" + 
						"#eureka.client.registerWithEureka=true\n" + 
						"#e#ureka.client.fetchRegistry=true\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/\n" + 
						"#eureka.instance.preferIpAddress= true\n" + 
						"\n" + 
						"\n" + 
						"#If you want to set your MQTT cluster set both options to true\n" + 
						"eureka.client.register-with-eureka=false\n" + 
						"eureka.client.fetch-registry=false\n" + 
						"\n" + 
						"spring.main.allow-bean-definition-overriding=true");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
