package io.moquette.spi.impl;

import static io.moquette.databaseAccessANDHTTPMethods.ConfigClass.PREFIX;
import static io.moquette.server.ConnectionDescriptor.ConnectionState.DISCONNECTED;
import static io.moquette.server.ConnectionDescriptor.ConnectionState.ESTABLISHED;
import static io.moquette.server.ConnectionDescriptor.ConnectionState.INIT_SESSION;
import static io.moquette.server.ConnectionDescriptor.ConnectionState.MESSAGES_REPUBLISHED;
import static io.moquette.server.ConnectionDescriptor.ConnectionState.SENDACK;
import static io.netty.channel.ChannelFutureListener.CLOSE_ON_FAILURE;
import static io.netty.channel.ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE;
import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.CONNECTION_ACCEPTED;
import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD;
import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.CONNECTION_REFUSED_IDENTIFIER_REJECTED;
import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.CONNECTION_REFUSED_UNACCEPTABLE_PROTOCOL_VERSION;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;

import io.moquette.connections.IConnectionsManager;
import io.moquette.databaseAccessANDHTTPMethods.ConfigClass;
import io.moquette.databaseAccessANDHTTPMethods.Credentials;
import io.moquette.databaseAccessANDHTTPMethods.CustomResponse;
import io.moquette.databaseAccessANDHTTPMethods.DatabaseUtils;
//import io.moquette.databaseAccessANDHTTPMethods.HTTPUtils;
import io.moquette.databaseAccessANDHTTPMethods.User;
import io.moquette.server.ConnectionDescriptor;
import io.moquette.server.netty.AutoFlushHandler;
import io.moquette.server.netty.NettyUtils;
import io.moquette.spi.ClientSession;
import io.moquette.spi.ISessionsStore;
import io.moquette.spi.impl.subscriptions.ISubscriptionsDirectory;
import io.moquette.spi.impl.subscriptions.Subscription;
import io.moquette.spi.impl.subscriptions.Topic;
import io.moquette.spi.security.IAuthenticator;
import io.moquette.spi.security.IAuthorizator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.mqtt.MqttConnAckMessage;
import io.netty.handler.codec.mqtt.MqttConnAckVariableHeader;
import io.netty.handler.codec.mqtt.MqttConnectMessage;
import io.netty.handler.codec.mqtt.MqttConnectPayload;
import io.netty.handler.codec.mqtt.MqttConnectReturnCode;
import io.netty.handler.codec.mqtt.MqttFixedHeader;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.handler.codec.mqtt.MqttVersion;
import io.netty.handler.timeout.IdleStateHandler;

@EnableAutoConfiguration
public class ConnectHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ConnectHandler.class);

	private IConnectionsManager connectionDescriptors;
	private BrokerInterceptor m_interceptor;
	private SessionsRepository sessionsRepository;
	private ISessionsStore m_sessionsStore;
	private IAuthenticator m_authenticator;
	private IAuthorizator m_authorizator;
	private ISubscriptionsDirectory subscriptions;
	private boolean allowAnonymous;
	private boolean allowZeroByteClientId;
	private boolean reauthorizeSubscriptionsOnConnect;
	private InternalRepublisher internalRepublisher;

	
	ConnectHandler(IConnectionsManager connectedClients, BrokerInterceptor interceptor,
			SessionsRepository sessionsRepository, ISessionsStore sessionsStore, IAuthenticator authenticator,
			IAuthorizator authorizator, ISubscriptionsDirectory subscriptions, boolean allowAnonymous,
			boolean allowZeroByteClientId, boolean reauthorizeSubscriptionsOnConnect,
			InternalRepublisher internalRepublisher) {
		this.connectionDescriptors = connectedClients;
		this.m_interceptor = interceptor;
		this.sessionsRepository = sessionsRepository;
		this.m_sessionsStore = sessionsStore;
		this.m_authenticator = authenticator;
		this.m_authorizator = authorizator;
		this.subscriptions = subscriptions;
		this.allowAnonymous = allowAnonymous;
		this.allowZeroByteClientId = allowZeroByteClientId;
		this.reauthorizeSubscriptionsOnConnect = reauthorizeSubscriptionsOnConnect;
		this.internalRepublisher = internalRepublisher;
	}

	public void processConnect(Channel channel, MqttConnectMessage msg) {
		MqttConnectPayload payload = msg.payload();
		String clientId = payload.clientIdentifier();
		
		final String username = payload.userName();
		LOG.debug("Processing CONNECT message. CId={}, username={}", clientId, username);

		if (isNotProtocolVersion(msg, MqttVersion.MQTT_3_1) && isNotProtocolVersion(msg, MqttVersion.MQTT_3_1_1)) {
			MqttConnAckMessage badProto = connAck(CONNECTION_REFUSED_UNACCEPTABLE_PROTOCOL_VERSION);

			LOG.error("MQTT protocol version is not valid. CId={}", clientId);
			channel.writeAndFlush(badProto).addListener(FIRE_EXCEPTION_ON_FAILURE);
			channel.close().addListener(CLOSE_ON_FAILURE);
			return;
		}

		final boolean cleanSession = msg.variableHeader().isCleanSession();
		if (clientId == null || clientId.length() == 0) {
			if (!cleanSession || !this.allowZeroByteClientId) {
				MqttConnAckMessage badId = connAck(CONNECTION_REFUSED_IDENTIFIER_REJECTED);

				channel.writeAndFlush(badId).addListener(FIRE_EXCEPTION_ON_FAILURE);
				channel.close().addListener(CLOSE_ON_FAILURE);
				LOG.error("MQTT client ID cannot be empty. Username={}", username);
				return;
			}

			// Generating client id.
			clientId = UUID.randomUUID().toString().replace("-", "");
			LOG.info("Client has connected with server generated id={}, username={}", clientId, username);
		}
		if(payload.userName().equals("test"))
			return;
			
		UUID uuid = UUID.randomUUID();
		clientId = uuid.toString();
		

		if (!login(channel, msg, clientId)) {
			
			LOG.error("invalid credentials were provided");
			return;
		}

		
		ConnectionDescriptor descriptor = new ConnectionDescriptor(clientId, channel, cleanSession);
		final ConnectionDescriptor existing = this.connectionDescriptors.addConnection(descriptor);
		if (existing != null) {
			LOG.info("Client ID is being used in an existing connection, force to be closed. CId={}", clientId);
			existing.abort();
			// return;
			this.connectionDescriptors.removeConnection(existing);
			this.connectionDescriptors.addConnection(descriptor);
		}

		if (!descriptor.assignState(DISCONNECTED, INIT_SESSION)) {
			channel.close().addListener(CLOSE_ON_FAILURE);
			return;
		}

		LOG.debug("Initializing client session {}", clientId);
		ClientSession existingSession = this.sessionsRepository.sessionForClient(clientId);
		boolean isSessionAlreadyStored = existingSession != null;
		final boolean msgCleanSessionFlag = msg.variableHeader().isCleanSession();
		if (isSessionAlreadyStored && msgCleanSessionFlag) {
			for (Subscription existingSub : existingSession.getSubscriptions()) {
				this.subscriptions.removeSubscription(existingSub.getTopicFilter(), clientId);
			}
		}

		initializeKeepAliveTimeout(channel, msg, clientId);
		final ClientSession clientSession = this.sessionsRepository.createOrLoadClientSession(clientId, cleanSession);
		clientSession.storeWillMessage(msg, clientId);
		int flushIntervalMs = 500/* (keepAlive * 1000) / 2 */;
		setupAutoFlusher(channel, flushIntervalMs);
		

		if (!cleanSession && reauthorizeSubscriptionsOnConnect) {
			reauthorizeOnExistingSubscriptions(clientId, username);
		}

		if (!descriptor.assignState(INIT_SESSION, SENDACK)) {
			channel.close().addListener(CLOSE_ON_FAILURE);
			return;
		}
		LOG.debug("Sending CONNACK. CId={}", clientId);
		MqttConnAckMessage okResp = createConnectAck(msg, clientId);

		final String connectClientId = clientId;

		descriptor.writeAndFlush(okResp, new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) {
				if (future.isSuccess()) {
					LOG.debug("CONNACK has been sent. CId={}", connectClientId);

					if (!descriptor.assignState(SENDACK, MESSAGES_REPUBLISHED)) {
						channel.close().addListener(CLOSE_ON_FAILURE);
						return;
					}
					m_interceptor.notifyClientConnected(msg);
					if (!msg.variableHeader().isCleanSession()) {
						// force the republish of stored QoS1 and QoS2
						internalRepublisher.publishStored(clientSession);
					}

					if (!descriptor.assignState(MESSAGES_REPUBLISHED, ESTABLISHED)) {
						channel.close().addListener(CLOSE_ON_FAILURE);
					}

					LOG.info("Connected client <{}> with login <{}>", connectClientId, username);
				} else {
					future.channel().pipeline().fireExceptionCaught(future.cause());
				}
			}
		});
	}

//    public void processConnect(Channel channel, MqttConnectMessage msg) {
//        MqttConnectPayload payload = msg.payload();
//        String clientId = payload.clientIdentifier();
//        final String username = payload.userName();
//        LOG.debug("Processing CONNECT message. CId={}, username={}", clientId, username);
//
//        if (isNotProtocolVersion(msg, MqttVersion.MQTT_3_1) && isNotProtocolVersion(msg, MqttVersion.MQTT_3_1_1)) {
//            MqttConnAckMessage badProto = connAck(CONNECTION_REFUSED_UNACCEPTABLE_PROTOCOL_VERSION);
//
//            LOG.error("MQTT protocol version is not valid. CId={}", clientId);
//            channel.writeAndFlush(badProto).addListener(FIRE_EXCEPTION_ON_FAILURE);
//            channel.close().addListener(CLOSE_ON_FAILURE);
//            return;
//        }
//
//        final boolean cleanSession = msg.variableHeader().isCleanSession();
//        if (clientId == null || clientId.length() == 0) {
//            if (!cleanSession || !this.allowZeroByteClientId) {
//                MqttConnAckMessage badId = connAck(CONNECTION_REFUSED_IDENTIFIER_REJECTED);
//
//                channel.writeAndFlush(badId).addListener(FIRE_EXCEPTION_ON_FAILURE);
//                channel.close().addListener(CLOSE_ON_FAILURE);
//                LOG.error("MQTT client ID cannot be empty. Username={}", username);
//                return;
//            }
//
//            // Generating client id.
//            clientId = UUID.randomUUID().toString().replace("-", "");
//            LOG.info("Client has connected with server generated id={}, username={}", clientId, username);
//        }
//
//        if (!login(channel, msg, clientId)) {
//            channel.close().addListener(CLOSE_ON_FAILURE);
//            return;
//        }
//
//        ConnectionDescriptor descriptor = new ConnectionDescriptor(clientId, channel, cleanSession);
//        final ConnectionDescriptor existing = this.connectionDescriptors.addConnection(descriptor);
//        if (existing != null) {
//            LOG.info("Client ID is being used in an existing connection, force to be closed. CId={}", clientId);
//            existing.abort();
//            //return;
//            this.connectionDescriptors.removeConnection(existing);
//            this.connectionDescriptors.addConnection(descriptor);
//        }
//
//        if (!descriptor.assignState(DISCONNECTED, INIT_SESSION)) {
//            channel.close().addListener(CLOSE_ON_FAILURE);
//            return;
//        }
//
//        LOG.debug("Initializing client session {}", clientId);
//        ClientSession existingSession = this.sessionsRepository.sessionForClient(clientId);
//        boolean isSessionAlreadyStored = existingSession != null;
//        final boolean msgCleanSessionFlag = msg.variableHeader().isCleanSession();
//        if (isSessionAlreadyStored && msgCleanSessionFlag) {
//            for (Subscription existingSub : existingSession.getSubscriptions()) {
//                this.subscriptions.removeSubscription(existingSub.getTopicFilter(), clientId);
//            }
//        }
//
//        initializeKeepAliveTimeout(channel, msg, clientId);
//        final ClientSession clientSession = this.sessionsRepository.createOrLoadClientSession(clientId, cleanSession);
//        clientSession.storeWillMessage(msg, clientId);
//        int flushIntervalMs = 500/* (keepAlive * 1000) / 2 */;
//        setupAutoFlusher(channel, flushIntervalMs);
//
//        if (!cleanSession && reauthorizeSubscriptionsOnConnect) {
//            reauthorizeOnExistingSubscriptions(clientId, username);
//        }
//
//        if (!descriptor.assignState(INIT_SESSION, SENDACK)) {
//            channel.close().addListener(CLOSE_ON_FAILURE);
//            return;
//        }
//        LOG.debug("Sending CONNACK. CId={}", clientId);
//        MqttConnAckMessage okResp = createConnectAck(msg, clientId);
//
//        final String connectClientId = clientId;
//
//        descriptor.writeAndFlush(okResp, new ChannelFutureListener() {
//            @Override
//            public void operationComplete(ChannelFuture future) {
//                if (future.isSuccess()) {
//                    LOG.debug("CONNACK has been sent. CId={}", connectClientId);
//
//                    if (!descriptor.assignState(SENDACK, MESSAGES_REPUBLISHED)) {
//                        channel.close().addListener(CLOSE_ON_FAILURE);
//                        return;
//                    }
//                    m_interceptor.notifyClientConnected(msg);
//                    if (!msg.variableHeader().isCleanSession()) {
//                        // force the republish of stored QoS1 and QoS2
//                        internalRepublisher.publishStored(clientSession);
//                    }
//
//                    if (!descriptor.assignState(MESSAGES_REPUBLISHED, ESTABLISHED)) {
//                        channel.close().addListener(CLOSE_ON_FAILURE);
//                    }
//
//                    LOG.info("Connected client <{}> with login <{}>", connectClientId, username);
//                } else {
//                    future.channel().pipeline().fireExceptionCaught(future.cause());
//                }
//            }
//        });
//    }

	private boolean isNotProtocolVersion(MqttConnectMessage msg, MqttVersion version) {
		return msg.variableHeader().version() != version.protocolLevel();
	}

	private void reauthorizeOnExistingSubscriptions(String clientId, String username) {
		if (!m_sessionsStore.contains(clientId)) {
			return;
		}
		final Collection<Subscription> clientSubscriptions = m_sessionsStore.subscriptionStore()
				.listClientSubscriptions(clientId);
		for (Subscription sub : clientSubscriptions) {
			final Topic topicToReauthorize = sub.getTopicFilter();
			final boolean readAuthorized = m_authorizator.canRead(topicToReauthorize, username, clientId);
			if (!readAuthorized) {
				subscriptions.removeSubscription(topicToReauthorize, clientId);
			}
		}
	}

	private void setupAutoFlusher(Channel channel, int flushIntervalMs) {
		try {
			channel.pipeline().addAfter("idleEventHandler", "autoFlusher",
					new AutoFlushHandler(flushIntervalMs, TimeUnit.MILLISECONDS));
		} catch (NoSuchElementException nseex) {
			// the idleEventHandler is not present on the pipeline
			channel.pipeline().addFirst("autoFlusher", new AutoFlushHandler(flushIntervalMs, TimeUnit.MILLISECONDS));
		}
	}

	private MqttConnAckMessage connAck(MqttConnectReturnCode returnCode) {
		return connAck(returnCode, false);
	}

	private MqttConnAckMessage connAckWithSessionPresent(MqttConnectReturnCode returnCode) {
		return connAck(returnCode, true);
	}

	private MqttConnAckMessage connAck(MqttConnectReturnCode returnCode, boolean sessionPresent) {
		MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE,
				false, 0);
		MqttConnAckVariableHeader mqttConnAckVariableHeader = new MqttConnAckVariableHeader(returnCode, sessionPresent);
		return new MqttConnAckMessage(mqttFixedHeader, mqttConnAckVariableHeader);
	}
	
	private boolean login(Channel channel, MqttConnectMessage msg, final String clientId) {
		// handle user authentication

		boolean isDevice = true;
		
		if(msg.payload().userName().length()>40) {
			
			final String password = new String (msg.payload().passwordInBytes(), Charset.forName("UTF-8"));
			
			
			User user = null;
			
			try{
				if(PREFIX.length()>1)
		    		user = ConfigClass.deviceAPIProxyEureka.verifyJWT(msg.payload().userName());
				else
					user = ConfigClass.deviceAPIProxyNoEureka.verifyJWT(msg.payload().userName());
//				user = ConfigClass.deviceAPIProxy.verifyJWT(msg.payload().userName());
				} catch(Exception ex){
				  user = null;
			}
			
			if(user == null) {
				try{
					
					if(PREFIX.length()>1)
			    		user = ConfigClass.adminAPIProxyEureka.verifyJWT(msg.payload().userName());
					else
						user = ConfigClass.adminAPIProxyNoEureka.verifyJWT(msg.payload().userName());
//				user = ConfigClass.adminAPIProxy.verifyJWT(msg.payload().userName());
				} catch(Exception ex){
					  user = null;
				}
				
				if(user == null) {
				
					failedCredentials(channel);
					return false;
				}
				isDevice = false;
			}
			
			final String login = msg.payload().userName();
			
			NettyUtils.userName(channel, login);

			DatabaseUtils databaseUtils = new DatabaseUtils();
			Connection connection = databaseUtils.getDBConnection();
			databaseUtils.createTableDevices(connection);

			String userName;
			if(isDevice)
				userName = user.getUsername();
			else
				userName = user.getEmail();
			int noBroadcast;
			if(password.equals("noBroadcast"))
				noBroadcast = 1;
			else
				noBroadcast = 0;
			
//			System.out.println(password);
//			System.out.println("NoBroadcast " + noBroadcast);
			
			databaseUtils.insertDataDevices(clientId,userName, "", user.getApplication(), login, connection, noBroadcast);
			
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return true;
			
		}
		else 
		if (!msg.variableHeader().hasUserName() && !msg.variableHeader().hasPassword()) {
			LOG.error("Client didn't supply any credentials and MQTT anonymous mode is disabled. CId={}", clientId);
			failedCredentials(channel);
			return false;
		}
		
		final String password = new String (msg.payload().passwordInBytes(), Charset.forName("UTF-8"));
		final String login = msg.payload().userName();
//		HTTPUtils httpUtils = new HTTPUtils();
//		CustomResponse response = null;
		
		
		Credentials credentials = new Credentials();
		
		ResponseEntity<String> response2;
		

//		if(login.startsWith(brokerUser)) {
//			
//			
//			
//			if(!login.equals(brokerUser+brokerUsername) || !password.equals(brokerPassword))
//				return false;
//			
//			NettyUtils.userName(channel, login);
//
//			System.out.println("I got here?");
//			DatabaseUtils databaseUtils = new DatabaseUtils();
//			Connection connection = databaseUtils.getDBConnection();
//			databaseUtils.createTableDevices(connection);
//			//Tem que alterar linha abaixo
//			//databaseUtils.insertDataDevices(login, password, "broker", connection);
//			databaseUtils.insertDataDevices(clientId,login, password, "broker", "token", connection);
//			try {
//				connection.close();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			return true;
//			
//		}
//		else 
		if(login.contains("@")) {
		
			
			credentials.setEmail(login);
			credentials.setPassword(password);


			if(PREFIX.length()>1)
				response2 = ConfigClass.adminAPIProxyEureka.login(credentials);
			else
				response2 = ConfigClass.adminAPIProxyNoEureka.login(credentials);

//			response2 = ConfigClass.adminAPIProxy.login(credentials);
			isDevice = false;
			
			
			
		} 
		else {
			System.out.println("I was in normal AUTH");
			credentials.setUsername(login);
			credentials.setPassword(password);
			
			if(PREFIX.length()>1)
				response2 = ConfigClass.deviceAPIProxyEureka.login(credentials);
			else
				response2 = ConfigClass.deviceAPIProxyNoEureka.login(credentials);
			
//			response2 = ConfigClass.deviceAPIProxy.login(credentials);
		}
		
		if (response2.getStatusCodeValue() != 200) {
			failedCredentials(channel);
			return false;
		}

		String token = response2.getHeaders().getFirst("Authorization");
		
		NettyUtils.userName(channel, login);

		JSONObject jsonObj = new JSONObject(response2.getBody());
		String application = jsonObj.getString("application");
		DatabaseUtils databaseUtils = new DatabaseUtils();
		Connection connection = databaseUtils.getDBConnection();
		databaseUtils.createTableDevices(connection);

		databaseUtils.insertDataDevices(clientId,login, "", application, token, connection,0);
		
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("I concluded normal AUTH");
		
		return true;
			
			
			
			

			/*
			 * if (!m_authenticator.checkValid(clientId, login, pwd)) { LOG.
			 * error("Authenticator has rejected the MQTT credentials CId={}, username={}",
			 * clientId, login); failedCredentials(channel); return false; }
			 * NettyUtils.userName(channel, login);
			 */

		/*
		 * 
		 * Original code else if (!this.allowAnonymous) { LOG.
		 * error("Client didn't supply any credentials and MQTT anonymous mode is disabled. CId={}"
		 * , clientId); failedCredentials(channel); return false; }
		 */
		// return true;

		/*
		 * if (msg.variableHeader().hasUserName()) { byte[] pwd = null; if
		 * (msg.variableHeader().hasPassword()) { pwd = msg.payload().passwordInBytes();
		 * } else if (!this.allowAnonymous) { LOG.
		 * error("Client didn't supply any password and MQTT anonymous mode is disabled CId={}"
		 * , clientId); failedCredentials(channel); return false; } final String login =
		 * msg.payload().userName(); if (!m_authenticator.checkValid(clientId, login,
		 * pwd)) { LOG.
		 * error("Authenticator has rejected the MQTT credentials CId={}, username={}",
		 * clientId, login); failedCredentials(channel); return false; }
		 * NettyUtils.userName(channel, login); } else if (!this.allowAnonymous) { LOG.
		 * error("Client didn't supply any credentials and MQTT anonymous mode is disabled. CId={}"
		 * , clientId); failedCredentials(channel); return false; } return true;
		 */
	}

	private MqttConnAckMessage createConnectAck(MqttConnectMessage msg, String clientId) {
		MqttConnAckMessage okResp;
		ClientSession clientSession = this.sessionsRepository.sessionForClient(clientId);
		boolean isSessionAlreadyStored = clientSession != null;
		final boolean msgCleanSessionFlag = msg.variableHeader().isCleanSession();
		if (!msgCleanSessionFlag && isSessionAlreadyStored) {
			okResp = connAckWithSessionPresent(CONNECTION_ACCEPTED);
		} else {
			okResp = connAck(CONNECTION_ACCEPTED);
		}
		return okResp;
	}

	private void initializeKeepAliveTimeout(Channel channel, MqttConnectMessage msg, String clientId) {
		int keepAlive = msg.variableHeader().keepAliveTimeSeconds();
		NettyUtils.keepAlive(channel, keepAlive);
		NettyUtils.cleanSession(channel, msg.variableHeader().isCleanSession());
		NettyUtils.clientID(channel, clientId);
		int idleTime = Math.round(keepAlive * 1.5f);
		setIdleTime(channel.pipeline(), idleTime);

		LOG.debug("Connection has been configured CId={}, keepAlive={}, removeTemporaryQoS2={}, idleTime={}", clientId,
				keepAlive, msg.variableHeader().isCleanSession(), idleTime);
	}

	private void failedCredentials(Channel session) {
		session.writeAndFlush(connAck(CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD))
				.addListener(FIRE_EXCEPTION_ON_FAILURE);
		LOG.info("Client {} failed to connect with bad username or password.", session);
	}

	private void setIdleTime(ChannelPipeline pipeline, int idleTime) {
		if (pipeline.names().contains("idleStateHandler")) {
			pipeline.remove("idleStateHandler");
		}
		pipeline.addFirst("idleStateHandler", new IdleStateHandler(idleTime, 0, 0));
	}
}
