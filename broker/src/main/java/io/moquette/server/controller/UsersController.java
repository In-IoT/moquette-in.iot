package io.moquette.server.controller;

import static io.moquette.databaseAccessANDHTTPMethods.ConfigClass.PREFIX;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.moquette.databaseAccessANDHTTPMethods.ConfigClass;
import io.moquette.databaseAccessANDHTTPMethods.DatabaseUtils;
//import io.moquette.databaseAccessANDHTTPMethods.HTTPUtils;
import io.moquette.databaseAccessANDHTTPMethods.User;
import io.moquette.model.UserList;
import io.moquette.server.Server;
import io.moquette.spi.impl.subscriptions.Subscription;

@RestController
public class UsersController {

    @RequestMapping("/listConnectedDevices")
    public UserList listConnectedDevices(@RequestHeader String token) {
    	
    	User user = new User();
		
    	if(PREFIX.length()>1)
    		user = ConfigClass.deviceAPIProxyEureka.verifyJWT(token);
		else
			user = ConfigClass.deviceAPIProxyNoEureka.verifyJWT(token);
    	
//		user = ConfigClass.deviceAPIProxy.verifyJWT(token);
    	
    	//HTTPUtils httpUtils = new HTTPUtils();
    	
    	//User user = httpUtils.getUserInfoFromJWT(token);
    	
    	if(user == null)
    		return null;
    	
    	DatabaseUtils databaseUtils = new DatabaseUtils();
		Connection connection = databaseUtils.getDBConnection();
		List<User> users = databaseUtils.selectClientsByApplication(user.getApplication(), connection);

		
		UserList user2 = new UserList(users)  ;

    	
        return user2;
    }
    
    @RequestMapping("/listSubscriptionsFromUsername")
    public UserList greeting(@RequestHeader String token, @RequestHeader String username) {
//    public UserList greeting(){
    	
    	User user = new User();
    	
    	if(PREFIX.length()>1)
    		user = ConfigClass.deviceAPIProxyEureka.verifyJWT(token);
		else
			user = ConfigClass.deviceAPIProxyNoEureka.verifyJWT(token);
		
//		user = ConfigClass.deviceAPIProxy.verifyJWT(token);
    	if(user == null)
    		return null;
    	
    	DatabaseUtils databaseUtils = new DatabaseUtils();
		Connection connection = databaseUtils.getDBConnection();
		
		List<User> users = databaseUtils.selectClientsByApplicationAndUsername(user.getApplication(), username, connection);
		Server srv = new Server();
		
		for(int i = 0; i<users.size(); i++) {
			
			Set<Subscription> subs = srv.getProcessor()
					.getSessionsRepository()
					.sessionForClient(
							users.get(i).getClientID())
					.getSubscriptions();
			List<String> subscriptionList = new ArrayList<>();
			for (Iterator<Subscription> iterator = subs.iterator(); iterator.hasNext();) {
				Subscription subscription = iterator.next();
				subscriptionList.add(subscription.getTopicFilter().toString());
			}
			
			users.get(i).setSubscriptionList(subscriptionList);

			
		}
		
		
//		sessionsRepository.sessionForClient(clientID).getSubscriptions().forEach(
//				session -> System.out.println("Subscribed Topic " + session.getTopicFilter().toString())
//				
//				);

		
		UserList user2 =new UserList(users)  ;
		
    	
    	//srv.getProcessor().getSessionsStore().subscriptionStore().listClientSubscriptions(clientID)
		
//		List<User> users = databaseUtils.selectClientsByApplication("root@root.com", connection);
//		
//		for (User user : users) {
//			//user.get
//		}
//		users.forEach(
//		
//				user -> System.out.println("ClientId " + user.getClientID() + "Username "+user.getUsername())
//				);
//    	
//    	Server srv = new Server();
//    	
//    	srv.getProcessor().getSessionsStore().subscriptionStore().listClientSubscriptions(clientID)
//    	
//    	srv.getProcessor().getConnectionDescriptors().getConnectedClientIds().forEach(
//				
//				connectedClientIds -> System.out.println("ConnectedClientIds " + connectedClientIds)
//				);
    	
    	
        return user2;
    }
    
    
    
    @RequestMapping("/listSubscriptionsFromClientId")
    public User listSubscriptionsFromClient(@RequestHeader String token, @RequestHeader String clientId) {
//    public UserList greeting(){
    	
    	User user = new User();
		
    	if(PREFIX.length()>1)
    		user = ConfigClass.deviceAPIProxyEureka.verifyJWT(token);
		else
			user = ConfigClass.deviceAPIProxyNoEureka.verifyJWT(token);
    	
//		user = ConfigClass.deviceAPIProxy.verifyJWT(token);
    	if(user == null)
    		return null;
    	
    	DatabaseUtils databaseUtils = new DatabaseUtils();
		Connection connection = databaseUtils.getDBConnection();
		
		user = databaseUtils.selectClientsByApplicationAndClientId(user.getApplication(), clientId, connection);
		Server srv = new Server();
		
			
			Set<Subscription> subs = srv.getProcessor()
					.getSessionsRepository()
					.sessionForClient(
							user.getClientID())
					.getSubscriptions();
			List<String> subscriptionList = new ArrayList<>();
			for (Iterator<Subscription> iterator = subs.iterator(); iterator.hasNext();) {
				Subscription subscription = iterator.next();
				subscriptionList.add(subscription.getTopicFilter().toString());
			}
			
			user.setSubscriptionList(subscriptionList);
    	
        return user;
    }
    
}
